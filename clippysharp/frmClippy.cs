using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace clippysharp
{
    public partial class frmClippy : Form
    {
        frmMsg frmMsg = new frmMsg();
        public static Random random = new Random();

        protected override CreateParams CreateParams
        {
            get
            {
                // Turn on WS_EX_TOOLWINDOW style bit
                CreateParams cp = base.CreateParams;
                cp.ExStyle |= 0x80;
                return cp;
            }
        }

        public frmClippy()
        {
            InitializeComponent();
        }

        private void frmClippy_Load(object sender, EventArgs e)
        {
            GlobalValues globals = new GlobalValues();
            idleTimer.Enabled = true;
            TransparencyKey = Color.Fuchsia;
            this.ShowInTaskbar = false;
            this.Location = globals.clippyXY;

            frmMsg.Show();
        }

        private void picClippy_Click(object sender, EventArgs e)
        {
            frmMsg.lblTxt = "Ouch. Don't click my face please";
        }

        //BEGIN CONTEXT MENU
        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }
        private void animateToolStripMenuItem_Click(object sender, EventArgs e)
        {
            idleTimer_Tick(sender, e);
        }
        //END CONTEXT MENU

        private void idleTimer_Tick(object sender, EventArgs e)
        {
            idleTimer.Enabled = false;
            int animationIndex = random.Next(1, 4);
            clippyAnimate(animationIndex);
        }

        private void clippyAnimate(int animationIndex)
        {

            switch (animationIndex)
            {
                default:
                    break;
                case 1: //idle blink
                    picClippy.Image = Properties.Resources.clippylook;
                    animationTimer.Interval = 1400;
                    break;
                case 2: //melt
                    picClippy.Image = Properties.Resources.clippymelt;
                    animationTimer.Interval = 5000;
                    soundThread("swoorl", 2, 2500);
                    break;
                case 3: //clippy tap
                    picClippy.Image = Properties.Resources.clippytap;
                    animationTimer.Interval = 1500;
                    soundThread("lighttap", 3, 0);
                    break;
                case 4: //clippy headphones
                    picClippy.Image = Properties.Resources.clippyheadphones;
                    animationTimer.Interval = 1800;
                    soundThread("alertslowdown", 1, 0);
                    break;
            }


            //start the animation timer
            animationTimer.Enabled = true;

        }

        private void animationTimer_Tick(object sender, EventArgs e)
        {
            //stop the animation
            animationTimer.Enabled = false;
            picClippy.Image = Properties.Resources.clippystandby;
            idleTimer.Interval = 15000 + random.Next(1, 20000);
            idleTimer.Enabled = true;

        }

        private void soundThread(string path, int count, int delay)
        {

            Thread sound = new Thread(() => playSound(path, count, delay));
            sound.Start();

        }
        private void playSound(string path, int count, int delay)
        {
            System.Threading.Thread.Sleep(delay);
            if (count == 1)
            {
                string soundPath = @"sounds/" + path + ".wav";
                System.Media.SoundPlayer sound = new System.Media.SoundPlayer(soundPath);
                sound.Play();
            }
            else
            {
                for (int i = 0; i < count; i++)
                {
                    i++;
                    string soundPath = @"sounds/" + path + ".wav";
                    System.Media.SoundPlayer sound = new System.Media.SoundPlayer(soundPath);
                    sound.PlaySync();
                }
            }
        }
    }
}
