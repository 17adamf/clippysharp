﻿namespace clippysharp
{
    partial class frmMsg
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.lblMsgTxt = new System.Windows.Forms.Label();
            this.btnOk = new System.Windows.Forms.Button();
            this.msgTimer = new System.Windows.Forms.Timer(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // pictureBox1
            // 
            this.pictureBox1.ErrorImage = null;
            this.pictureBox1.Image = global::clippysharp.Properties.Resources.mesbox;
            this.pictureBox1.InitialImage = null;
            this.pictureBox1.Location = new System.Drawing.Point(12, 12);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(139, 134);
            this.pictureBox1.TabIndex = 0;
            this.pictureBox1.TabStop = false;
            // 
            // lblMsgTxt
            // 
            this.lblMsgTxt.AutoSize = true;
            this.lblMsgTxt.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(204)))));
            this.lblMsgTxt.Font = new System.Drawing.Font("Consolas", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblMsgTxt.Location = new System.Drawing.Point(21, 23);
            this.lblMsgTxt.MaximumSize = new System.Drawing.Size(120, 0);
            this.lblMsgTxt.Name = "lblMsgTxt";
            this.lblMsgTxt.Size = new System.Drawing.Size(116, 82);
            this.lblMsgTxt.TabIndex = 1;
            this.lblMsgTxt.Text = "It looks like you\'re trying to make a C# clippy. Would you like some help with th" +
    "at?";
            this.lblMsgTxt.UseCompatibleTextRendering = true;
            this.lblMsgTxt.Click += new System.EventHandler(this.lblMsgTxt_Click);
            // 
            // btnOk
            // 
            this.btnOk.BackColor = System.Drawing.Color.LemonChiffon;
            this.btnOk.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnOk.Font = new System.Drawing.Font("Consolas", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnOk.ForeColor = System.Drawing.Color.Black;
            this.btnOk.Location = new System.Drawing.Point(90, 102);
            this.btnOk.Name = "btnOk";
            this.btnOk.Size = new System.Drawing.Size(52, 21);
            this.btnOk.TabIndex = 2;
            this.btnOk.Text = "Ok";
            this.btnOk.UseVisualStyleBackColor = false;
            this.btnOk.Click += new System.EventHandler(this.btnOk_Click);
            // 
            // msgTimer
            // 
            this.msgTimer.Interval = 15000;
            this.msgTimer.Tick += new System.EventHandler(this.msgTimer_Tick);
            // 
            // frmMsg
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Fuchsia;
            this.ClientSize = new System.Drawing.Size(162, 158);
            this.Controls.Add(this.btnOk);
            this.Controls.Add(this.lblMsgTxt);
            this.Controls.Add(this.pictureBox1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "frmMsg";
            this.Text = "clippy text";
            this.TopMost = true;
            this.Load += new System.EventHandler(this.frmMsg_Load);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Label lblMsgTxt;
        private System.Windows.Forms.Button btnOk;
        private System.Windows.Forms.Timer msgTimer;
    }
}