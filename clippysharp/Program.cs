using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Drawing;
using System.Diagnostics;

namespace clippysharp
{
    public class GlobalValues
    {

        static frmClippy frmClippy = new frmClippy();
        public Point clippyXY = new Point(Screen.PrimaryScreen.WorkingArea.Width-frmClippy.Width, Screen.PrimaryScreen.WorkingArea.Height-frmClippy.Height);
    }
    static class Program
    {
        [STAThread]
        static void Main()
        {

            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new frmClippy());

        }
    }
}
