﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace clippysharp
{
    public partial class frmMsg : Form
    {

        private static Random random = new Random();
        private static System.Media.SoundPlayer popupsound = new System.Media.SoundPlayer(@"sounds/balloon.wav");
        protected override CreateParams CreateParams
        {
            get
            {
                // Turn on WS_EX_TOOLWINDOW style bit
                CreateParams cp = base.CreateParams;
                cp.ExStyle |= 0x80;
                return cp;
            }
        }
        public string lblTxt
        {
            get { return lblMsgTxt.Text; }
            set { lblMsgTxt.Text = value; }
        }
        public frmMsg()
        {
            InitializeComponent();
        }

        private void frmMsg_Load(object sender, EventArgs e)
        {
            
            GlobalValues globals = new GlobalValues();
            Point msgboxXY = new Point(globals.clippyXY.X - 40, globals.clippyXY.Y - this.Height + 20);
            this.Location = msgboxXY;
            TransparencyKey = Color.Fuchsia;
            this.ShowInTaskbar = false;
            newMsg();
        }

        private void btnOk_Click(object sender, EventArgs e)
        {
            Hide();
            msgTimer.Enabled = true;

        }
        public void newMsg()
        {

            List<string> msgContent = new List<string>();
            StreamReader msgtext = new StreamReader("texts/msgs.txt", System.Text.Encoding.ASCII);
            string currentLine;
            currentLine = msgtext.ReadLine();

            while (currentLine != null)
            {
                msgContent.Add(currentLine.ToString());
                currentLine = msgtext.ReadLine();
            }
            int msgIndex = random.Next(1, msgContent.Count);
            lblMsgTxt.Text = msgContent[msgIndex];
            popupsound.Play();
            
        }

        private void msgTimer_Tick(object sender, EventArgs e)
        {
            msgTimer.Enabled = false;
            msgTimer.Interval = 17000 + random.Next(1, 25000);
            lblMsgTxt.Text = "";
            Show();
            newMsg();
        }

        private void lblMsgTxt_Click(object sender, EventArgs e)
        {

        }
    }
}
